// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCIcbJmQA0Y5ea88PuRWcrMuPMLmUixi_U",
    authDomain: "example2-6459d.firebaseapp.com",
    projectId: "example2-6459d",
    storageBucket: "example2-6459d.appspot.com",
    messagingSenderId: "675191186519",
    appId: "1:675191186519:web:5e1c1137d61d73c3b12f12"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
