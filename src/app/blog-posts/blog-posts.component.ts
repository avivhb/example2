import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { BlogPostsService } from '../blog-posts.service';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {

  posts$:Observable<Post>
  
  constructor(private blogPostsService:BlogPostsService) { }

  ngOnInit(): void {
    this.posts$ = this.blogPostsService.searchPosts();
  }

}
