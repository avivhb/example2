import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  userId:string;
  userMail:string;
  

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
            user => {
              this.userId = user.uid;
              this.userMail = user.email; 
                }
              )
            }
  }


